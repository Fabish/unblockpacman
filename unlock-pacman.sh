#! /bin/sh
# This is a helper script for the systemd-service "unlock-pacman".

if [ -f "/var/lib/pacman/db.lck" ]
    then
        rm "/var/lib/pacman/db.lck"
        printf "pacman lock was found and removed"
fi
